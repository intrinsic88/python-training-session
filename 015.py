import socket
HOST = ''
PORT = 50007
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(5)
clientSock, addr = s.accept()
print("Connect from:", addr)
while True:
    data = clientSock.recv(1024)
    clientSock.sendall(data)
    clientSock.close()