import MySQLdb

connection = MySQLdb.connect("localhost", "root", "avendata", "hackerrank")

cursor = connection.cursor()
allTables = cursor.execute("SHOW TABLES like 'EMP';")
print(allTables)
if(allTables<1):
    cursor.execute("CREATE TABLE EMP(name TEXT, age INTEGER);")

cursor.execute("INSERT INTO EMP(name,age) VALUES ('blah1', 26);")

cursor = connection.cursor()
cursor.execute("SELECT name,age FROM EMP;")
results = cursor.fetchall()
for row in results:
    print(row)

cursor.close()
connection.commit()
connection.close()