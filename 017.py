import configparser

config = configparser.ConfigParser()
config.read("config.ini")

print(config.sections())

username = config.get("client", "user")
password = config.get("client", "password")

print(username, password)
