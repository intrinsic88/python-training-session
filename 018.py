import re

input = "This is an input string"
rex = r'.*is.*'

#matches = re.match(rex, input)
matches = re.match(r'(.*)is(.*)', input)

if matches:
    print("Matched")
    print(matches.group(1))
    print(matches.group(2))

else:
    print("Not matched")

input1 = "this is a string #this is a comment"
matches1 = re.sub(r'#.*', ".", input1)

print(matches1)

input2 = "this is a string.this is a comment"
matches2 = re.sub(r'/..*', ",", input2)

print(matches2)

