class MyException(BaseException):
    pass

def fun():
    try:
        #x = 1/0
        raise MyException
    except ZeroDivisionError:
        print("ZeroError")
    except MyException:
        print("user-error")
    print("done")

fun()