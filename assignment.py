import socket
import re
HOST = ''
PORT = 8080
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(5)
while True:
    clientSock, addr = s.accept()
    print("Connect from:", addr)
    data = clientSock.recv(1024)
    string = bytes.decode(data)
    request_method = string.split(' ')[0]
    request_url = string.split(' ')[1]
    filename = re.match(r'(.*)/(.*)', request_url)
    fname = filename.group(2)
    with open(fname, 'rb') as f:
        response = f.read()

    header = 'HTTP/1.1 200 OK\n'
    clientSock.send(header.encode('utf-8'))
    clientSock.send('Content-Type:text/html <strong>\n\n</strong>'.encode('utf-8'))
    clientSock.send(response)
    clientSock.close()
s.close()