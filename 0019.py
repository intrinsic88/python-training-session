#decorators

def fun1(x):
    print("Fn1:" + x)



def fn_decor(df):
    def wrapper(x):
        print("Before fun1")
        df(x)
        print("after fun1")

    return wrapper
fun1("abc")

fun1 = fn_decor(fun1)

fun1("bcd")

@fn_decor
def newFun1(x):
    print("new fun1")

newFun1("fgh")