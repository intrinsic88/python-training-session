import functools
def add(x,y):
    return x+y

z = add(10,11)
print(z)


add1 = lambda x,y: x+y
z1 = add1(10,11) 
print(z1)

in_arr = [12,11,10,9,8,7,6,78]

out_ar=map(lambda x:x+10, in_arr)
out_ar = list(out_ar)

out_array = [ x+10 for x in in_arr]
print(out_ar)
print(out_array)

odd = filter(lambda x: x%2 != 0, in_arr)
odd = list(odd)
odd1 = [x for x in in_arr if(x%2 != 0)]

print(odd)
print(odd1)


def sub(x,y):
    return y-x

subRes = functools.reduce(lambda x,y:sub(x,y), in_arr)

subRes = subRes

print(subRes)

