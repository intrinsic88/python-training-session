import time
from threading import Thread

MAX_TIME = 500000000

x = 1
def counter(max):
    while (max > 0):
        max -= 1

#thread1 = Thread(target=counter, args=(MAX_TIME,))
#thread2 = Thread(target=counter, args=(MAX_TIME,))

start = time.time()
counter(MAX_TIME)

#thread1.start()
#thread2.start()
#thread1.join()
#thread2.join()
end = time.time()
print("time:", end - start)