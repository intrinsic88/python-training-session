#pickle 
import pickle

employee = {"name":"blah", "age":24}

with open("employee.pp", "wb") as f:
    pickle.dump(employee,f)

with open("employee.pp", "rb") as f:
    new_employee = pickle.load(f)

print(new_employee)

class pickleTest:
    def __init__(self, name):
        self.name = name

t1 = pickleTest("xyz")

with open("ptest.pp", "wb") as f:
    pickle.dump(t1,f)

with open("ptest.pp", "rb") as f:
    new_t1 = pickle.load(f)

print(new_t1.name)
