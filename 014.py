#f = open("file009.py", "w")
#(w)rite implies read and write
#(r)ead in readonly
#(x) create a file if it doesn't exist
#c = f.read()/f.write()
#c = f.readline()
#c = f.read(10)
#for line in f
#   line = line.strip("\n")
#   print(line)
#for binary use rb and wb 
#append is a+ 
with open("file009.py", "r") as f:
    c = f.readline()
    print(c)

with open("File10.py", "x") as x:
    c = x.write("#Write this \n def fn1():\n \t print('This is a function')")
#f.close()
